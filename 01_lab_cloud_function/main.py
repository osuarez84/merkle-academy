# using SendGrid's Python Library
# https://github.com/sendgrid/sendgrid-python
import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail


def send_welcome_email(data, context):
    SENDGRID_API_KEY = os.getenv('SENDGRID_API_KEY')

    html_content = f"""<p>Dear customer, </p> \
                    <br> \
                    <p>An event of type {context.event_type} has\
                    ocurred in the bucket {data['bucket']} </p> \
                    <p>The name of the file is {data['name']}.</p>\
                    <br>"""

    # TODO
    # Set your email here!!!
    message = Mail(
        from_email='my@example.com',
        to_emails='<your-email>@example.es',
        subject='Monitoring of events.',
        html_content=html_content)
    
    
    try:
        sg = SendGridAPIClient(SENDGRID_API_KEY)
        response = sg.send(message)
        print(response.status_code)
        print(response.body)
        print(response.headers)
    except Exception as e:
        print(e)


if __name__ == "__main__":
    send_welcome_email('test', 'test')